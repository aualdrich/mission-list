class NewsletterMailer < ActionMailer::Base
  default from: 'info@missionlist.org' #todo: change this to the user's email

  def newsletter_mail(newsletter_id, user_id)
	@newsletter = Newsletter.get(newsletter_id)
	@items = []
	@emails = []

	TripNewsletterRecipients.all_trip_newsletter_recipients.each do |x|
		if x.user_id == user_id && x.trip_id == @newsletter.trip_id
			x.recipient_ids.each do |r_id|
				recipient = NewsletterRecipient.get(r_id)

				if(recipient.email)
					@emails << recipient.email	
				end
			end
				
		end

	end

	@newsletter.newsletter_items.each do |item_id|
		item = JournalEntry.get(item_id)
		@items << item
	end

	mail(to: @emails, subject: @newsletter.name)
  end

end
