 class WelcomeMailer < MandrillMailer::TemplateMailer
 	default from: 'info@missionlist.org'

	 def welcome(recipient_email, first_name)
	    mandrill_mail template: 'welcome-email',
	    	 subject: "Welcome to Mission List!",
	         to: {email: recipient_email, name: first_name}
	 end
 end