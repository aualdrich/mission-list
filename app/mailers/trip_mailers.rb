class TripMailers < ActionMailer::Base
  default from: 'info@missionlist.org'


  def send_follow_notification(to_addresses, follower_name, trip)
  	@trip = trip
  	@follower_name = follower_name
	mail(to: to_addresses, subject: "#{follower_name} is now following your trip!")
  end

end
