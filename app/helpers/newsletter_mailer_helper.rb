module NewsletterMailerHelper
	include PhotosHelper


	def entry_date_formatter(entry_date)
		return entry_date.strftime("%a, %b %d %Y") if entry_date != nil		
		return ""		
	end


end
