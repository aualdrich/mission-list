module PhotosHelper
	include ApplicationHelper

	def remote_photo_url(photo_id, is_thumb)
		base_url = "http://s3.amazonaws.com/missionjournal"
		return "#{base_url}/thumbnails/#{photo_id}.jpg" if is_thumb
		return "#{base_url}/#{photo_id}.jpg"
	end


	def remote_photo(photo_id)
		begin
			s3_url = remote_photo_url(photo_id, false)
			puts "<img src='#{s3_url}></img>"

			rescue S3::Error::NoSuchKey
				
		end
	end


end