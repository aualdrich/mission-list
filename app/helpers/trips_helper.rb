module TripsHelper

	def trip_date(input)		
		return input.strftime("%-b. %-d, %Y") if input != nil		
		return ""
	end

	def team(trip)
		team_with_or_without_me(trip, true)
  end

  def team_with_or_without_me(trip, with_me) 
      teammate_ids = []

      current_user_id = current_user.nil? ? -1 : current_user.id

      unless trip.user_id == current_user_id and !with_me
        teammate_ids << trip.user_id 
      end

      unless trip.teammate_user_ids.count == 0
        trip.teammate_user_ids.each do |user_id|
          unless user_id == current_user_id and !with_me
            teammate_ids << user_id
          end
        end
      end

      teammates = []

      teammate_ids.each do |teammate_id|
        team_member = User.get(teammate_id)
        teammates << team_member unless team_member.nil?
      end

      return teammates

  end

    def team_without_me(trip) 
      return get_team_with_or_without_me(trip, false)
    end

    def load_teammate_from_list(user_id, teammates) 
      return teammates.first { |t| t.id == user_id}
    end

end
