require 's3'

module ApplicationHelper	

	def flash_class(level)
	    case level
	        when :notice then "alert alert-info"
	        when :success then "alert alert-success"
	        when :error then "alert alert-error"
	        when :alert then "alert alert-error"
	    end
	end


	def s3_bucket
		service = S3::Service.new(:access_key_id => ENV['AWS_ACCESS_KEY'],
                          :secret_access_key => ENV['AWS_SECRET_KEY'])

		bucket = service.buckets.find("missionjournal")

	end

	def get_valid_photo_ids(potential_photo_ids)			
		valid_photo_ids = []

		s3 = AWS::S3.new
		bucket = s3.buckets['missionjournal']
		
		potential_photo_ids.each do |photo_id|
			 if bucket.objects["#{photo_id}.jpg"].exists?
				valid_photo_ids << photo_id		
			 end				
		end

		return valid_photo_ids
	

	end

	def paragraph_limiter(text)
		if text.length > 500
			text.first(497) + "..."
		else
			text
		end

	end

	def back_button(back_text)
		"<a class='btn btn-default' onClick='history.back()'>#{back_text}</a>".html_safe
	end

  	def nav_link_to(link_name, route, controller)
    	
    	css_class = "active" if params[:controller] == controller
    	"<li class='#{css_class}'><a href='#{route}'>#{link_name}</a></li>".html_safe    
	end

	def fb_share(object_type, url)

		namespace_prefix = ENV['FACEBOOK_NAMESPACE_PREFIX']

		"FB.ui({
			
					  method: 'share_open_graph',
					  action_type: '#{namespace_prefix}:share',
					  action_properties: JSON.stringify({
					      #{object_type}: '#{url}',
					  })
					}, function(response){});
		".html_safe

	end

end
