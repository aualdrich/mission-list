class NewsletterController < ApplicationController
	before_filter :authenticate_api, :only => 'send_newsletter'
	protect_from_forgery with: :null_session

	def new

	end

	def manage_newsletters
		@trip = Trip.get(params[:trip_id])

	end


	def show
		@newsletter = Newsletter.get(params[:id])
	@items = []

	@newsletter.newsletter_items.each do |item_id|
		item = JournalEntry.get(item_id)
		@items << item
	end	

		render "newsletter_mailer/newsletter_mail", :layout => false
	end

	def send_newsletter
		@newsletter_id = params[:newsletter_id]
		@user_id = params[:user_id]

		NewsletterMailer.newsletter_mail(@newsletter_id, @user_id).deliver

		render :json => {:success => true, :message => 'Newsletter sent!'}
	end

end
