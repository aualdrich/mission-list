class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user, :user_is_trip_owner, :user_is_team_member, :user_id, :friends, :user_token, :user_owns_journal_entry, :profile_info, :user_has_stripe_account,
    :nullable_current_user_id, :get_team, :user_is_team_member, :user_is_following_trip, :user_belongs_to_trip, 
    :ensure_team_member, :ensure_trip_owner, :ensure_correct_team_member, :authenticate_api

  include FBInfo, TripsHelper

  private
    def authenticate_api
      authenticate_or_request_with_http_basic do |username, password|
        username == ENV['API_USER'] && password == ENV['API_PASSWORD'] 
      end
    end

    def profile_info(user_id)
      return get_info_for_profile(user_id)
    end

    def current_user
        @current_user ||= session[:current_user] if session[:current_user]
    end

    def user_id
      current_user.id
    end

    def user_token
      session[:user_token]
    end

    def user_is_trip_owner(trip, user=current_user)
       return false if user.nil?
    	 trip.user_id == user.id
    end

    def user_is_team_member(trip, user=current_user)

      return false if user.nil?

    	return true if user_is_trip_owner(trip, user)
    	    	
    	trip.teammate_user_ids.each do |user_id|
    		if user_id == user.id
    			return true
    		end
    	end

    	return false

    end

    def user_owns_journal_entry(entry)
      return false if current_user.nil?

      puts 'got here'

      return entry.user_id == current_user.id

    end

    def get_team(trip)
      get_team_with_or_without_me(trip, true)
    end

    def get_team_with_or_without_me(trip, with_me) 
      teammates = team_with_or_without_me(trip, with_me)

      teammate_ids = teammates.map {|t| t.id}

      return get_friends(teammate_ids)    
    end

    def get_team_without_me(trip) 
      return get_team_with_or_without_me(trip, false)
    end

    def user_has_stripe_account(user_id)
      user = User.get(user_id)
      return false if user.nil?
      return !user.stripe_access_token.nil? && user.stripe_access_token.length > 0;
    end

    def nullable_current_user_id
       current_user.nil? ? "" : current_user.id
    end

    def user_is_following_trip(trip)
      return false if current_user.nil?
      trips_followed = current_user.trips_followed
      return false if trips_followed.nil?
      return trips_followed.include?(trip.id)
    end

    def user_belongs_to_trip(trip)
      user_is_trip_owner(trip) || user_is_team_member(trip)
    end

    def ensure_trip_owner
      trip = Trip.get(params[:trip_id])
      redirect_to root_path unless user_is_trip_owner(trip)
    end

    def ensure_correct_team_member
       entry = JournalEntry.get(params[:id])
       redirect_to root_path unless user_owns_journal_entry(entry)
    end

    def ensure_team_member
      trip = Trip.get(params[:trip_id])
      redirect_to root_path unless user_belongs_to_trip(trip)
    end
    
end

