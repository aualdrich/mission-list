class WelcomeController < ApplicationController	
	before_filter :authenticate_api, :only => 'send_welcome_email'
	protect_from_forgery with: :null_session

	def index
		#redirect_to trip_feed_path if !current_user.nil?

	end


	def send_welcome_email
		to_address = params[:to_address]
		first_name = params[:to_first_name]

		WelcomeMailer.welcome(to_address, first_name).deliver

		render :json => {:success => true, :message => 'Welcome email sent!'}

	end

end

