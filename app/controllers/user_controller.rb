
class UserController < ApplicationController
	def show
		@profile_info = profile_info(params[:id])

		Trip.public_trips.each do |t|

			is_team_member = t.user_id == @profile_info.id

			t.teammate_user_ids.each do |user_id|

	    		if user_id == @profile_info.id
	    			is_team_member = true
	    			break
	    		end
			end

			@profile_info.trips << t if is_team_member

		end

	end

	def my_account
		@donation = Donation.new
		@user = current_user
	end

	

end
