class SessionsController < ApplicationController
	def create
		    uid = auth_hash[:uid]
		    first_name = auth_hash[:info][:first_name]
		    last_name = auth_hash[:info][:last_name]
		    photo_url = auth_hash[:info][:image]
		    email = auth_hash[:info][:email]

		    user = User.get(uid)

		    if(user == nil)
		    	user = User.new(:id => uid, :first_name => first_name, :last_name => last_name, 
		    		:photo_url => photo_url, :email => email, :doc_type => 'fb_user')
		    	user.save
          WelcomeMailer.welcome(email, first_name).deliver
          
		    else
		    	user.update_attributes(:first_name => first_name, :last_name => last_name, 
		    		:photo_url => photo_url, :email => email)
		    end

		    session[:current_user] = user
		    session[:user_token] = auth_hash[:credentials][:token]	   


        if user.stripe_user_id.nil? or user.stripe_user_id.length == 0
          redirect_to connect_to_stripe_path

        else
          redirect_to view_my_trips_path  
        end

		    
  	end

  	def create_stripe  		
  		
      access_token = auth_hash[:credentials][:token]
      stripe_user_id = auth_hash[:uid]
      stripe_publishable_key = auth_hash[:info][:stripe_publishable_key]

  		user = current_user
  		user.update_attributes(:stripe_access_token => access_token, :stripe_user_id => stripe_user_id, :stripe_publishable_key => stripe_publishable_key)
  		
  		redirect_to my_account_path

  	end

  	def signup
  		

  	end

  	def signin

  	end

  	def destroy
  		session[:current_user] = nil;
  		redirect_to root_path
  	end

  	def destroy_stripe
  		current_user.update_attributes({:stripe_user_id => '', :stripe_access_token => '', :stripe_publishable_key => ''})	
  		redirect_to my_account_path
  	end

  protected
	  def auth_hash
	    request.env['omniauth.auth']
	  end
end
