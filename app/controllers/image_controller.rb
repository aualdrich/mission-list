class ImageController < ApplicationController

	def resize_images
		s3 = AWS::S3.new
		bucket = s3.buckets['missionjournal']
		
		bucket.objects.each do |o|		
			begin
				key = o.key
				new_key = "thumbnails/#{key}"
				url = "https://s3.amazonaws.com/missionjournal/#{key}"
				puts url

				#ensure thumbnail doesn't already exist
				unless bucket.objects[new_key].exists?

					unless o.content_length == 0 
						image = MiniMagick::Image.open(url)
						image.resize "100x100"
						image.write key
						bucket.objects.create new_key, image.to_blob
					end

				end

			rescue
				next
			end
		end

	end

end