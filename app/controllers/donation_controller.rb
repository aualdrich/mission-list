class DonationController < ApplicationController

	def new 
		@donation = Donation.new(
			:amount => 10,
			:to_user_id => params[:to_user_id]
		)

		@profile_info = profile_info(@donation.to_user_id)
		to_user = User.get(params[:to_user_id])
		@stripe_key = to_user.stripe_publishable_key
		@specific_trip_id = params[:trip_id].nil? ? "" : params[:trip_id]
		
		redirect_to show_user_path(params[:to_user_id]) if !user_has_stripe_account(params[:to_user_id])

	end

	def get_new_donation_data
		to_user_id = params[:to_user_id]
		to_user = User.get(to_user_id)

		trips = []
		Trip.all_trips.each do |t|			
			trips << t if user_is_team_member(t, to_user)
		end

		parts = trips.partition { |t| t.start_date.nil? }
		trips = parts.last.sort_by {|t| t.start_date }.reverse + parts.first

		render :json => {:trips => trips}
	end

	def new_trip
		@trip_id = params[:trip_id]
		@trip = Trip.get(@trip_id)

		@team = get_team(@trip)


		render 'trip_donation'
	end

	def save 
		payment_amount = params[:amount] #note this is in cents for stripe
		to_user = User.get(params[:to_user_id])
		donor_email = params[:donor_email]
		donation_message = params[:message]
		donor_from_full_name = current_user.nil? ? "" : "#{current_user.first_name} #{current_user.last_name}"
		stripe_fee_description = build_stripe_fee_description(donation_message, donor_from_full_name, donor_email)
		trip_id = params[:trip_id]

		@donation = Donation.new(:donor_user_id => nullable_current_user_id, :donor_email => donor_email, :amount => cents_to_dollars(payment_amount),
			:message => donation_message, :date => Time.new, :to_user_id => params[:to_user_id],
			:doc_type => 'donation', :stripe_token => params[:token_id], :donor_name => donor_from_full_name, :trip_id => trip_id
		)
		
		begin
			
			charge = create_stripe_charge(to_user, payment_amount, @donation.stripe_token, stripe_fee_description)	

			if @donation.save!
				render :json => {:success => true, :message => 'Donation made successfully!', :redirect_location => donation_complete_url}
				return
			else
				render :json => {:success => false, :message => "Sorry, but your donation couldn't be saved." }	
			end
		  
		rescue Exception => e						
		    render :json => {:success => false, :message => "#{e.message}" }
		    return
		end

		render :json => {:success => false, :message => "We're sorry, but something went wrong."}

	end

	def donation_complete
	end

	def update_thank_you_message
		current_user.update_attributes(:thank_you_message => params[:user][:thank_you_message])
		redirect_to my_account_path
	end

	def donation_error
		@error_message = params[:message]
	end

	def view_my_donations
		@donations = []

		Donation.all_donations.each do |d| 
			 @donations << d if d.to_user_id == current_user.id
		end

		@donations = @donations.sort_by {|d| d.date }.reverse

		@total_raised = @donations.sum { |d| d.amount.to_d }
	end

	def test_stripe
		
	end

	def cents_to_dollars(amount)
		amount / 100
	end

	def calculate_application_fee(amount)
		raw_amount = amount * 0.02		
		puts raw_amount.round
		return raw_amount.round #needs to be a whole number for stripe
	end

	def create_stripe_charge(to_user, payment_amount, payment_token, fee_description)
		 Stripe.api_key = to_user.stripe_access_token		

		  charge = Stripe::Charge.create(
		    :amount => payment_amount,
		    :currency => "usd",
		    :card => @donation.stripe_token,
		    :description => fee_description,
		    :application_fee => calculate_application_fee(payment_amount)
		  )
	end

	def build_stripe_fee_description(donation_message, from_name, from_email)
		from_text = from_name.length > 0 ? from_name : from_email
		message = donation_message.nil? ? "" : donation_message

		"Donation from #{from_text}: #{message}"

	end




		# def get_trip_donation_data
	# 	@trip = Trip.get(params[:trip_id])
	# 	@teammates = get_team_without_me(@trip)

	# 	@teammates.each do |t|
	# 		user_record = User.get(t.id.to_s)
	# 		t.stripe_user_id = user_record.stripe_user_id unless user_record.nil?
	# 	end

	# 	render :json => {trip: @trip, teammates: @teammates}
	# end


	# def make_trip_donation
	# 	to_user_ids = []
	# 	receiver_list = []

	# 	params[:teammates].each do |t|
	# 		user = User.get(t[:id].to_s)
	# 		payment_amount = t[:amount]

	# 		user_donation = Donation.new(:donor_user_id => current_user.id, :amount => cents_to_dollars(payment_amount),
	# 		:message => donation_message, :date => Time.new, :to_user_id => params[:to_user_id],
	# 		:doc_type => 'donation', :stripe_token => params[:stripe_token]
	# 		)

	# 		user_donation.save!
	# 		to_user_ids << user_donation.id

	# 		charge = create_stripe_charge(user, payment_amount, user_donation.stripe_token)	

	# 	end

	# 	trip_donation = TripDonation.new(:trip_id => params[:trip_id], :donor_user_id => current_user.id,
	# 		:total_amount => params[:total_amount], :date => Time.new, :to_user_ids => to_user_ids, :doc_type => 'trip_donation')
			
	# 	trip_donation.save!

	# end

	private
		def donation_params
			params.require(:donation).permit!
		end



end
