class AdminController < ApplicationController

	def do_action
		#if Rails.env.development? 
			WelcomeMailer.welcome('aualdrich@gmail.com', 'Austin').deliver
		#end	
	end

	def remove_photos_not_in_s3

		if Rails.env.development? 
				
			JournalEntry.entries_for_trip.each do |e|

					s3 = AWS::S3.new
					bucket = s3.buckets['missionjournal']

					final_ids = []	
					excluded_ids = []			
					
					e.photo_ids.each do |photo_id|
						 if bucket.objects["#{photo_id}.jpg"].exists?
							final_ids << photo_id
						 else
						 	excluded_ids << photo_id
						 end				
					end					

					e.update_attributes(:photo_ids => final_ids)
				end

		end


	end

	# def delete_doc
	# 	if Rails.env.development? 
	# 		doc_id = params[:doc_id]
	# 		doc = JournalEntry.get(doc_id)
	# 		doc.destroy
	# 	end
	# end

end
