class FindTripsController < ApplicationController		
	include ApplicationHelper

	def index
		@trips = []
	end

	def search		
		@trips = []
		@search_term = ""
		render :partial => "search"

	end

	def perform_search
		@search_term = params[:search_text]
		search_query = "trip_name:#{@search_term}* or team_name:#{@search_term}*"
		request_url = "#{ENV['CLOUDANT_URL']}/#{ENV['CLOUDANT_DB']}/_design/trips/_search/trip_search?q=#{search_query}"
		encoded_request_url = URI.encode(request_url)

		response = JSON.parse HTTParty.get(encoded_request_url, 
			:basic_auth => {:username => ENV['CLOUDANT_USER'], :password => ENV['CLOUDANT_PASSWORD']})

		@trips = []

		if response["rows"] != nil
			response["rows"].each do |t|
				trip_id = t["id"]
				trip = Trip.get(trip_id)	
				next if trip.nil?

				trip_owner = User.get(trip.user_id)
				next if trip_owner.nil? or trip_owner.doc_type == 'anonymous_user'

				trip.num_journal_entries = get_num_journal_entries_for_trip(trip)
				@trips << trip				

			end


		end

		render "_search"
	end

	def get_num_journal_entries_for_trip(trip)
		entry_count = 0
		JournalEntry.entries_for_trip.each do |j|
			entry_count +=1 if j.trip_id == trip.id
		end

		return entry_count
	end

end