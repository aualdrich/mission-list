class JournalEntryController < ApplicationController
	include ApplicationHelper

	before_filter :ensure_correct_team_member, :only => [:edit, :delete, :update]
	before_filter :ensure_team_member, :only => [:new]


	def show
		journal_entry_id = params[:id]

		@entry = JournalEntry.get(journal_entry_id) 

		@tags = []

		unless @entry.tag_ids.nil?
			@entry.tag_ids.each do |tag_id|
				@tags << Tag.get(tag_id)			
			end
		end
		
		@trip = Trip.get(@entry.trip_id)

	end

	def get_photo_ids_for_entry		
		entry = JournalEntry.get(params[:journal_entry_id])
		photo_ids = get_valid_photo_ids(entry.photo_ids)

		render :json => {:photo_ids => photo_ids}

	end

	def new
		@trip = Trip.find(params[:trip_id])
		@id = -1
		@url = save_journal_entry_path(@trip.id)
		@page_title = "New Entry"

		render :action => "new_edit"
	end

	def edit		
		entry = JournalEntry.find(params[:id])
		@id = entry.id
		@trip = Trip.get(entry.trip_id)
		@url = update_journal_entry_path(params[:id])
		@page_title = "New Entry"

		render :action => "new_edit"
	end

	def save		
		entry_params = get_entry_params	
		entry = JournalEntry.new(entry_params)	
		entry.tag_ids = assign_tags(params[:tags])
		
		entry.save!

		redirect_url = show_journal_entry_url(entry.id)		
		render :json => {:redirect_url => redirect_url}

	end

	def delete
		entry = JournalEntry.get(params[:id])
		entry.destroy

		redirect_to show_trip_path(entry.trip_id)
	end

	def update
		entry_params = get_entry_params	
		entry = JournalEntry.get(params[:id])	
		entry.tag_ids = assign_tags(params[:tags])
		
		entry.update_attributes(entry_params)

		redirect_url = show_journal_entry_url(entry.id)		
		render :json => {:redirect_url => redirect_url}
	end

	def assign_tags(tags)
		tag_ids = []
		
		unless tags.nil?
			tags.each do |t|
				if t[:id] == -1
					new_tag = Tag.new 
					new_tag.tag_name = t[:tag_name]
					new_tag.doc_type = "tag"
					new_tag.user_id = current_user.id

					new_tag.save
					tag_ids << new_tag.id
				else
					tag_ids << t[:id]
				end
			end
		end

		return tag_ids

	end


	def get_tags_for_user 
		tags = []
		Tag.all_tags.each do |t|
			tags << t if t.user_id == current_user.id
		end

		render :json => {:tags => tags}

	end

	def get_entry_info
		entry = JournalEntry.get(params[:id])

		tags = []

		unless entry.tag_ids.nil? 
			entry.tag_ids.each do |tag_id|
				tags << Tag.get(tag_id)
			end
		end

		render :json => {:entry => entry, :tags => tags}

	end

	private

		def get_entry_params
			param = params.require(:entry).permit!
			param.merge({:user_id => current_user.id, :doc_type => 'journal_entry'})
		end



end
