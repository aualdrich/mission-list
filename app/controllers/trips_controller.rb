class TripsController < ApplicationController		
	include ApplicationHelper
	include TripsHelper
	before_filter :authenticate, :only => 'send_welcome_email'
	protect_from_forgery with: :null_session

	before_filter :ensure_trip_owner, :only => [:manage_trip, :manage_team, :delete_team_member, :update_trip]

	def index

	end

	def show
		trip_id = params[:trip_id]

		@journal_entries = []
		@trip = Trip.get(trip_id)

		puts "TRIP ID: #{trip_id}"
		puts @trip.trip_name

		@teammates = get_team(@trip)

		JournalEntry.entries_for_trip.each do |j|
			@journal_entries << j if j.trip_id == trip_id && (!j.is_private || j.user_id == nullable_current_user_id)
		end

		@journal_entries = @journal_entries.sort_by{|j| j[:entry_date] }.reverse
	
	end

	def get_photo_ids_for_trip
		trip_id = params[:trip_id]

		journal_entries = []

		JournalEntry.entries_for_trip.each do |j|
			journal_entries << j if j.trip_id == trip_id
		end

		potential_photo_ids = []

		journal_entries.each do |j|
			potential_photo_ids = potential_photo_ids + j.photo_ids
		end

		#photo_ids = get_valid_photo_ids(potential_photo_ids)

		render :json => {:photo_ids => potential_photo_ids}

	end

	def view_my_trips
		@trips = []
		@trips_following = []

		Trip.all_trips.each do |t|			
			@trips << t if user_is_team_member(t)
			@trips_following << t if user_is_following_trip(t)
		end

	end

	def manage_trip
		@trip = Trip.get(params[:trip_id])
		@url = update_trip_path(@trip)
	end

	def manage_team
		@trip = Trip.get(params[:trip_id])
		@teammates = get_team_without_me(@trip)
	end

	def add_trip
		@trip = Trip.new
		@url = save_trip_path		
		render :manage_trip
	end

	def update_trip
		@trip = Trip.get(params[:trip_id])
		@trip.user_id = current_user.id		
		@trip.doc_type = "trip"

		if @trip.update_attributes(trip_attributes)
			redirect_to show_trip_path(@trip)
		else
			@url = update_trip_path(@trip)
			render "manage_trip"
		end

	end

	def save_trip
		@trip = Trip.new(trip_attributes)
		@trip.user_id = current_user.id
		@trip.doc_type = "trip"

		if @trip.save
			redirect_to show_trip_path(@trip)
		else
			@url = save_trip_path
			render :manage_trip

		end

	end

	def invite_friends
		trip = Trip.find(params[:trip_id])
		incoming_friend_ids = params[:friend_ids]

		teammate_user_ids = trip.teammate_user_ids;

		incoming_friend_ids.each do |id|
			unless trip.teammate_user_ids.include?(id)
				teammate_user_ids << id
			end

		end

		trip.update_attributes(:teammate_user_ids => teammate_user_ids)
		
		render :json => {success: true, teammate_user_ids: teammate_user_ids}
	end

	def delete_team_member
		trip = Trip.find(params[:trip_id])
		team_member_id = params[:team_member_id]

		puts 'deleting team member'
		puts team_member_id

		teammate_user_ids = trip.teammate_user_ids
		teammate_user_ids.delete(team_member_id)

		trip.update_attributes(:teammate_user_ids => teammate_user_ids)

		redirect_to manage_team_path(trip.id)

	end

	def follow_trip
		if(current_user.nil?)
			redirect_to :sign_up_to_receive_updates
		else
			puts params[:trip_id]
			trip = Trip.get(params[:trip_id])			
			trips_followed = current_user.trips_followed.nil? ? [] : current_user.trips_followed
			unless trips_followed.include?(trip.id)
				to_recipients = teammate_email_recipients(trip, current_user)

				if to_recipients.length > 0
					TripMailers.send_follow_notification(to_recipients, current_user.first_name, trip).deliver
				end

				trips_followed << trip.id

				puts trips_followed
				current_user.update_attributes(:trips_followed => trips_followed)
			end
			
			flash.notice = "You are now following #{trip.trip_name}!"

			redirect_to show_trip_path(params[:trip_id]) 

		end

	end

	def stop_following_trip
		trip_id = params[:trip_id]
		redirect_to show_trip_path(trip_id) if current_user.nil?

		trip = Trip.get(trip_id)

		trips_followed = current_user.trips_followed
		trips_followed.delete(trip_id)

		current_user.update_attributes(:trips_followed => trips_followed)

		flash.notice = "You are no longer following #{trip.trip_name}"
		redirect_to show_trip_path(trip_id)


	end

	def sign_up_to_receive_updates

	end

	def trip_feed
		@trips = []
		Trip.all_trips.each do |t|
			@trips << t if user_is_following_trip(t)
		end
	end

	def send_follower_notification_email
		trip = Trip.get(params[:trip_id])
		user = User.get(params[:user_id])

		to_recipients = teammate_email_recipients(trip, user)

		if to_recipients.length > 0
			TripMailers.send_follow_notification(to_recipients, user.first_name, trip).deliver
		end

		render :json => {:success => true, :message => 'Notification sent!'}
	end

	def teammate_email_recipients(trip, user)
		puts user.first_name
		teammates = team(trip, user)

		teammates.select { |t| !t.email.nil? and t.email.length > 0}.map { |t| t.email}
	end

	private
		def trip_attributes
			#not terribly fond of this hack, but it's the easiest way to convert the checkbox's weird way of
			#storing checked as "1" instead of true.
			is_public_trip = params[:trip][:is_public_trip]
			params[:trip][:is_public_trip] = is_public_trip == "1" ? true : false;

			params.require(:trip).permit!
		end

		def email_recipient_params
			params.require(:email_recipient).permit!
		end

		def authenticate
	    	authenticate_or_request_with_http_basic do |username, password|
		    username == ENV['API_USER'] && password == ENV['API_PASSWORD'] 
		end
	  end

end
