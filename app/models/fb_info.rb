require 'koala'

module FBInfo


	def get_profile_info
		return get_info_for_profile(user_id)
	end

	def get_info_for_profile(requested_user_id)

		@api = Koala::Facebook::API.new

		results = @api.fql_query("SELECT name, uid, pic_square from user where uid = #{requested_user_id}")

		return build_profile_from_query_result(results.first, requested_user_id)

	end

	def build_profile_from_query_result(query_result, user_id)
		profile_info = Profile.new
		profile_info.id = user_id
		profile_info.name = query_result["name"]
		profile_info.photo_url = query_result["pic_square"]

		return profile_info
	end

	def get_friends(friend_ids)

		return [] if friend_ids.empty?
	
		joined_friend_ids = friend_ids.join(",")

		query = "SELECT name, uid, pic_square from user where uid in (#{joined_friend_ids})"

		puts query
		
		@api = Koala::Facebook::API.new(user_token)
	
		results = @api.fql_query(query)

		unsorted = results.map {|x| build_profile_from_query_result(x, x["uid"])}

		sorted = unsorted.sort_by { |x| x.name }

		return sorted;

	end


end