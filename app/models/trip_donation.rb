class TripDonation < CouchRest::Model::Base

	property :trip_id, String
	property :donor_user_id, String
	property :total_amount, Integer
	property :date, DateTime
	property :to_user_ids, [String]
	property :doc_type, String

	design do
		view :all_trip_donations, :map => 
			"function(doc) { 
				if(doc['doc_type'] == 'trip_donation') { 
					emit (doc._id, doc);
				} 
			 }"
	end	


end