class Tag < CouchRest::Model::Base	
	property :tag_name, String
	property :user_id, String
	property :doc_type, String

	design do 
		view :all_tags, :map => 
			"function(doc) { 
				if(doc['doc_type'] == 'tag') {				
						emit (doc._id, doc);
				} 
			 }"

	end

end