class TripNewsletterRecipients < CouchRest::Model::Base
	property :trip_id, String
	property :user_id, String
	property :doc_type, String
	property :recipient_ids, [String]


	design do 
		view :all_trip_newsletter_recipients, :map => 
			"function(doc) {
				if(doc['doc_type'] == 'trip_newsletter_recipients')
					emit(doc._id, doc);
			}"
	end

end