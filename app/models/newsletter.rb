class Newsletter < CouchRest::Model::Base
	property :custom_message, String
	property :user_id, String
	property :newsletter_items, [String]
	property :doc_type, String
	property :trip_id, String
	property :trip_name, String
	property :date_created, DateTime
	property :name, String
end