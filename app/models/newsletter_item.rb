class NewsletterItem < CouchRest::Model::Base
	property :doc_type, String
	property :item_type, String
	property :entry_id, String
	property :entry_date, DateTime
	property :entry_date_formatted, String
	property :entry_description, String
	property :photo_src, String
	property :thumbnail_src, String
	property :photo_id, String

end