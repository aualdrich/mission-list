class User < CouchRest::Model::Base	
	property :doc_type, String
	property :first_name, String
	property :last_name, String
	property :photo_url, String
	property :email, String	
	property :stripe_user_id, String
	property :stripe_access_token, String
	property :stripe_publishable_key, String
	property :thank_you_message, String
	property :trips_followed, [String]


	design do 
		view :users_with_email, :map => 
			"function(doc) { 
				if(doc['doc_type'] == 'fb_user') {				
					if(doc['email']) {
						emit (doc._id, doc);
					}
				} 
			 }"

	end
end
