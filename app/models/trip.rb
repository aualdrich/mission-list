class Trip < CouchRest::Model::Base	
	include FBInfo

	validates_presence_of :trip_name
	validates_numericality_of :price, :greater_than_or_equal_to => 0

	property :trip_name, String
	property :team_name, String
	property :start_date, Date
	property :end_date, Date
	property :description, String	
	property :teammate_user_ids, [String]
	property :user_id, String
	property :is_public_trip, TrueClass, :default => true
	property :doc_type, String
	property :num_journal_entries, Integer	
	property :price, String
	property :contact_email, String

	design do
		view :public_trips, :map => 
			"function(doc) { 
				if(doc['doc_type'] == 'trip') { 
					if(doc['is_public_trip'])
						emit (doc._id, doc);
				} 
			 }"
		view :all_trips, :map => 
			"function(doc) { 
				if(doc['doc_type'] == 'trip') { 					
						emit (doc._id, doc);
				} 
			 }"
	end



end
