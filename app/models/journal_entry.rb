

class JournalEntry < CouchRest::Model::Base	
	include FBInfo

	property :trip_id, String
	property :user_id, String	
	property :entry_description, String
	property :entry_date, DateTime
	property :is_private, TrueClass
	property :photo_ids, [String]
	property :tag_ids, [String]
	property :doc_type, String

	attr_reader :user_token
	 
	design do 
		view :entries_for_trip, :map => 
			"function(doc) { 
				if(doc['doc_type'] == 'journal_entry') {				
						emit (doc._id, doc);
				} 
			 }"

	end

end
