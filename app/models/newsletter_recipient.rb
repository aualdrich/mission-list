class NewsletterRecipient < CouchRest::Model::Base
	property :first_name, String
	property :last_name, String
	property :email, String
	property :doc_type, String
end