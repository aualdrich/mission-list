class Profile

	def initialize
		@name = ""
		@photo_url = ""
		@id = ""
		@trips = []
		@stripe_user_id
	end

	attr_accessor :name, :photo_url, :id, :trips, :stripe_user_id

end