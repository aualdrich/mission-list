class Donation < CouchRest::Model::Base
	validates_presence_of :amount

	property :donor_user_id, String
	property :donor_name, String
	property :donor_email, String
	property :amount, Integer
	property :message, String
	property :date, DateTime
	property :to_user_id, String
	property :doc_type, String
	property :stripe_token, String
	property :trip_id, String


	design do
		view :all_donations, :map => 
			"function(doc) { 
				if(doc['doc_type'] == 'donation') { 
					emit (doc._id, doc);
				} 
			 }"
	end	


end