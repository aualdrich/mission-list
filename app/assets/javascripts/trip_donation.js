// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var underscore = angular.module('underscore', []);

underscore.factory('_', function() {
	return window._;
});

var app = angular.module('donation_app', ['underscore']);

app.directive('integer', function(){
    return {
        require: 'ngModel',
        link: function(scope, ele, attr, ctrl){
            ctrl.$parsers.unshift(function(viewValue){
                return parseInt(viewValue, 10);
            });
        }
    };
});

app.controller('DonationController', ['$scope', '$http', '_', '$q', function($scope, $http, _, $q, moment) {

	$scope.initialize = function(trip_id, stripe_key) {
		$scope.loading = true;
		$scope.trip_id = trip_id;
		$scope.total_amount = 0;
		$scope.get_data(trip_id);
		$scope.submitting = false;
		$scope.everyone_amount = 0;

		$scope.stripe_handler = StripeCheckout.configure({
			key: stripe_key,
			image: photo_url,
			token: function(token, args) {
				$scope.$apply(function() {
					$scope.make_donation(token);
				});
				
			}
		});

	};

	$scope.make_donation_clicked = function() {

	};

	$scope.get_data = function(trip_id) {
		$http.get('/donation/get_trip_donation_data/' + trip_id).success(function(result) {
			$scope.trip = result.trip;
			$scope.teammates = result.teammates;

			_.each($scope.teammates, function(t) {
				t.amount = 0;
			});

			$scope.loading = false;

			$scope.$watch("teammates", function(new_value, old_value) {
				$scope.recalculate_total_amount();
			}, true);

		});
	};

	$scope.everyone_amount_change = function() {
		_.each($scope.teammates, function(t) {
			if($scope.user_has_stripe_account(t)) {
				t.amount = $scope.everyone_amount;
			}
		});
	};

	$scope.recalculate_total_amount = function() {
		$scope.total_amount = 
			_.chain($scope.teammates)
			.map(function(t) {
				return t.amount;
			})
			.reduce(function(memo, i) { return memo + i;})
			.value();
	};


	$scope.make_donation = function() {
		$scope.submitting = true;

		var data = {
			trip_id: $scope.trip_id,
			total_amount: $scope.total_amount,
			teammates: _.filter($scope.teammates, function(t) {
				return $scope.user_has_stripe_account(t);
			})
		};

		$http.post('/donation/make_trip_donation', data).success(function(response) {
			window.location.href = response.return_url;
		}).error(function(response) {
			$scope.submitting = false;
		});
	};

	$scope.user_has_stripe_account = function(teammate) {
		return teammate.stripe_user_id && teammate.stripe_user_id.length > 0;
	};

	$scope.get_amount_title = function(teammate) {
		return $scope.user_has_stripe_account(teammate) ? "" : "User has not set up donations";
	};

}]);
