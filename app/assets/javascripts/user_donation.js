var underscore = angular.module('underscore', []);

underscore.factory('_', function() {
	return window._;
});

var user_donation_app = angular.module('user_donation_app', ['underscore']);

user_donation_app.directive('integer', function(){
    return {
        require: 'ngModel',
        link: function(scope, ele, attr, ctrl){
            ctrl.$parsers.unshift(function(viewValue){
                return parseInt(viewValue, 10);
            });
        }
    };
});

user_donation_app.controller('UserDonationController', ['$scope', '$http', '_', '$q', function UserDonationController($scope, $http, _, $q) {
	$scope.initialize = function(stripe_key, photo_url, user_name, to_user_id, designated_trip_id) {
		$scope.user_name = user_name;
		$scope.to_user_id = to_user_id;
		$scope.error_message = '';
		$scope.submitting = false;		
		$scope.trips_loading = true;

		$scope.stripe_handler = StripeCheckout.configure({
			key: stripe_key,
			image: photo_url,
			token: function(token, args) {
				$scope.$apply(function() {
					$scope.make_payment(token);
				});
				
			}
		});

		$http.get('/donation/get_new_donation_data/' + to_user_id).success(function(response) {
			$scope.trips = response.trips;
			$scope.trips_loading = false;

			if(designated_trip_id) {
				$scope.trip = _.find($scope.trips, function(t) { return t._id == designated_trip_id; });
			}

		});

	};

	$scope.formatted_trip_name = function(trip) {

		if(!trip.start_date)
			return trip.trip_name;

		var start_date = moment(trip.start_date).format("MMM YYYY");

		return trip.trip_name + " (" + start_date + ")";


	};

	$scope.payment_amount_in_cents = function() {
		return $scope.payment_amount * 100;
	};

	$scope.make_payment = function(token) {
		$scope.submitting = true;

		var trip_id = $scope.trip ? $scope.trip._id : "";

		var params = {
			token_id: token.id,
			amount: $scope.payment_amount_in_cents(),
			message: $scope.message,
			donor_email: token.email,
			trip_id: trip_id
		};

		$http.post('/donation/save/' + $scope.to_user_id, params)
			.success(function(response) {
				if(response.success) {
					window.location.href = response.redirect_location;
				}
				else {
					$scope.submitting = false;
					$scope.error_message = response.message;
				}

			});
	};

	$scope.pay_now = function() {
		$scope.stripe_handler.open({
			name: 'Donate to ' + $scope.user_name,
			description: $scope.message,
			amount: $scope.payment_amount_in_cents()
		});
	};

}]);
