// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.



var underscore = angular.module('underscore', []);

underscore.factory('_', function() {
	return window._;
});

var moment = angular.module('moment', []);

moment.factory('moment', function() {
	return window.moment;
});

var app = angular.module('journal_entry_app', ['ngTagsInput', 'underscore', 'moment']);


app.controller('JournalEntryController', ['$scope', '$http', '_', '$q', 'moment', function($scope, $http, _, $q, moment) {
	

	$scope.initialize = function(trip_id, id, url) {		
		$scope.DATE_FORMAT = "MM/DD/YYYY h:mm a";
		$scope.DATE_SAVE_FORMAT = "YYYY-MM-DD HH:mm:ss";
		$scope.available_tags = [];
		$scope.url = url;

		$http.get('/journal_entry/get_tags_for_user').then(function(response) {			
			$scope.available_tags = response.data.tags;
		});

		if(id == -1) {
			$scope.entry = {						
				id: -1,
				entry_date: moment().format($scope.DATE_FORMAT),
				trip_id: trip_id
			};
		}

		else {
			$http.get('/journal_entry/get_entry_info/' + id).then(function(result) {

				var entry = result.data.entry;

				$scope.entry = {
					id: entry._id,
					entry_date: moment(entry.entry_date).utc().format($scope.DATE_FORMAT),
					trip_id: entry.trip_id,
					entry_description: entry.entry_description
				};

				var selected_tags = $scope.map_tag_response_to_tags(result.data.tags);

				$scope.entry.tags = selected_tags;

			});
		}

	};

	$scope.add_tag = function(tag) {

		if(!tag.id)
			tag.id = -1;
	};

	$scope.load_tags = function(query) {

		var deferred = $q.defer();

		var results = _.filter($scope.available_tags, function(tag) {
			return tag.tag_name.indexOf(query) === 0;
		});

		var mapped_results = $scope.map_tag_response_to_tags(results);

		deferred.resolve(mapped_results);

		return deferred.promise;

	};

	$scope.map_tag_response_to_tags = function(results) {
		var mapped_results = _.map(results, function(tag) {
			return {tag_name: tag.tag_name, id: tag._id};
		});

		return mapped_results;
	};

	$scope.save = function(form) {
		if(form.$valid) {

			var entry_data = {
				trip_id: $scope.entry.trip_id,
				entry_description: $scope.entry.entry_description,
				entry_date: moment($scope.entry.entry_date).format($scope.DATE_SAVE_FORMAT),
				is_private: $scope.entry.is_private,				
			};

			$http.post($scope.url, {
				entry: entry_data,
				tags: $scope.entry.tags
			}).then(function(response) {
				window.location.href = response.data.redirect_url;
			});		


		}
	};




}]);

	