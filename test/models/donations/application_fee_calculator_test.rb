require 'test_helper'

class ApplicationFeeCalculatorTest < ActiveSupport::TestCase
	test "application fee calculates correctly" do
		fee_amount = 10.00
		expected_fee = 0.2
		output = ApplicationFeeCalculator.calculate(fee_amount)

		assert output == expected_fee
	end


  # test "the truth" do
  #   assert true
  # end
end
