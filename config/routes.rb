MissionList::Application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root 'welcome#index'
 
   resources :users, :user_session
   

   get 'find_trips/', to: 'find_trips#index', as: :find_trips
   get 'find_trips/search', to: 'find_trips#search', as: :trip_search
   post 'find_trips/perform_search', to: 'find_trips#perform_search', as: :perform_trip_search
   
   get 'trips/show/:trip_id' => 'trips#show', as: :show_trip
   get 'trips/photo_ids_for_trip' => 'trips#get_photo_ids_for_trip', as: :photo_ids_for_trip
   get 'trips/view_my_trips' => 'trips#view_my_trips', as: :view_my_trips
   get 'trips/manage_trip/:trip_id' => 'trips#manage_trip', as: :manage_trip         
   get 'trips/add_trip' => 'trips#add_trip', as: :add_trip
   patch 'trips/update_trip/:trip_id' => 'trips#update_trip', as: :update_trip
   post 'trips/save_trip' => 'trips#save_trip', as: :save_trip
   get 'trips/follow_trip/:trip_id' => 'trips#follow_trip', as: :follow_trip
   get 'trips/stop_following_trip/:trip_id' => 'trips#stop_following_trip', as: :stop_following_trip
   get 'trips/sign_up_to_receive_updates' => 'trips#sign_up_to_receive_updates', as: :sign_up_to_receive_updates
   get 'trips/trip_feed' => 'trips#trip_feed', as: :trip_feed
   get 'trips/manage_email_list/:trip_id' => 'trips#manage_email_list', as: :manage_email_list
   get 'trips/new_email_recipient/:trip_id' => 'trips#new_email_recipient', as: :new_email_recipient
   post 'trips/save_email_recipient/:trip_id' => 'trips#save_email_recipient', as: :save_email_recipient
   delete 'trips/delete_email_recipient/:id' => 'trips#delete_email_recipient', as: :delete_email_recipient
   post 'trips/send_follower_notification_email' => 'trips#send_follower_notification_email', as: :send_follower_notification_email

   #team
   get 'trips/manage_team/:trip_id' => 'trips#manage_team', as: :manage_team
   get 'trips/get_fb_friends' => 'trips#get_fb_friends', as: :get_fb_friends
   post 'trips/invite_friends' => 'trips#invite_friends', as: :invite_friends
   delete 'trips/delete_team_member/trip_id=:trip_id&team_member_id=:team_member_id' => 'trips#delete_team_member', as: :delete_team_member

   #newsletters
   scope '/trips/:trip_id' do 
     get 'newsletter/manage_newsletters' => 'newsletter#manage_newsletters', :as => :manage_newsletters
   end

   post 'newsletter/send_newsletter' => 'newsletter#send_newsletter', :as => :send_newsletter
   get 'newsletter/show/:id' => 'newsletter#show', :as => :show_newsletter

   get 'journal_entry/show/:id' => 'journal_entry#show', as: :show_journal_entry
   get 'journal_entry/photo_ids_for_entry' => 'journal_entry#get_photo_ids_for_entry', as: :photo_ids_for_entry
   get 'journal_entry/new/:trip_id' => 'journal_entry#new', as: :new_journal_entry
   get 'journal_entry/edit/:id' => 'journal_entry#edit', as: :edit_journal_entry
   post 'journal_entry/save' => 'journal_entry#save', as: :save_journal_entry
   get 'journal_entry/get_tags_for_user' => 'journal_entry#get_tags_for_user', as: :get_tags_for_user
   post 'journal_entry/update/:id' => 'journal_entry#update', as: :update_journal_entry
   get 'journal_entry/get_entry_info/:id' => 'journal_entry#get_entry_info', as: :get_entry_info
   delete 'journal_entry/delete/:id' => 'journal_entry#delete', as: :delete_journal_entry

   get 'images/resize_images' => 'image#resize_images', as: :resize_images


   get '/auth/facebook/callback', to: 'sessions#create'
   get '/auth/stripe_connect/callback', to: 'sessions#create_stripe'
   get 'signout' => 'sessions#destroy', as: :signout
   get 'signup' => 'sessions#signup', as: :signup
   get 'signin' => 'sessions#signin', as: :signin
   get 'disconnect_stripe' => 'sessions#destroy_stripe', as: :disconnect_stripe

   get 'user/show/:id' => 'user#show', as: :show_user
   get 'user/my_account' => 'user#my_account', as: :my_account
   get 'user/connect_to_stripe' => 'user#connect_to_stripe', as: :connect_to_stripe

   get 'donation/new/:to_user_id(/:trip_id)' => 'donation#new', as: :new_donation
   get 'donation/get_new_donation_data/:to_user_id' => 'donation#get_new_donation_data', as: :get_new_donation_data
   post 'donation/save/:to_user_id' => 'donation#save', as: :save_donation
   get 'donation/donation_complete' => 'donation#donation_complete', as: :donation_complete
   get 'donation/donation_error/:message' => 'donation#donation_error', as: :donation_error
   patch 'donation/update_thank_you_message' => 'donation#update_thank_you_message', as: :update_thank_you_message
   get 'donation/new_trip/:trip_id' => 'donation#new_trip', as: :new_trip_donation   
   get 'donation/get_trip_donation_data/:trip_id' => 'donation#get_trip_donation_data', as: :get_trip_donation_data
   post 'donation/make_trip_donation' => 'donation#make_trip_donation', as: :make_trip_donation
   get 'donation/view_my_donations' => 'donation#view_my_donations', as: :view_my_donations

   get 'admin/do_action' => 'admin#do_action', as: :do_action
   get 'admin/delete_doc/:doc_id' => 'admin#delete_doc'


   post 'welcome/send_welcome_email' => 'welcome#send_welcome_email', as: :send_welcome_email


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
