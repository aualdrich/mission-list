  ActionMailer::Base.smtp_settings = {
    :address   => "smtp.mandrillapp.com",
    :port      => 25, # ports 587 and 2525 are also supported with STARTTLS
    :enable_starttls_auto => true, # detects and uses STARTTLS
    :user_name => ENV['MANDRILL_USERNAME'],
    :password  => ENV["MANDRILL_API_KEY"], # SMTP password is any valid API key
    :authentication => 'login', # Mandrill supports 'plain' or 'login'
    :domain => 'www.missionlist.org', # your domain to identify your server when connecting
  }
  ActionMailer::Base.delivery_method = :smtp

  MandrillMailer.configure do |config| 
    config.api_key = ENV["MANDRILL_API_KEY"]
  end