Rails.application.config.middleware.use OmniAuth::Builder do  
  provider :facebook, ENV['FACEBOOK_KEY'], ENV['FACEBOOK_SECRET'],  {:provider_ignores_state => true, :scope => 'public_profile,email,user_friends'}
  provider :stripe_connect, ENV['STRIPE_CLIENT_ID'], ENV['STRIPE_SECRET_KEY'], :scope => 'read_write'

end